/// <reference types="cypress" />

context('SchlockDB Functional Test', () => {
    beforeEach(() => {

    })

    it('Header should be rendered correctly', () => {
        cy.server()
        cy.route('POST', 'http://localhost:3000/graphql', 'fixture:allMoviesFixture.json')
        cy.visit('http://localhost:8080')

        cy.get('#SHeader').should('be.visible');
    })

    it('Header should have the correct title', () => {
        cy.get('#SHeader-Title').contains('░▒▓ Ｓｃｈｌｏｃｋ ＤＢ ▓▒░');
    })

    it('Trending should have the correct title', () => {
        cy.get('.main-title').contains('Ｔｒｅｎｄｉｎｇ');
    })

    it('Should contain the movie "Miami Connection"', () => {
        cy.contains('Miami Connection').should('have.length', 1);
    })

    it('Should contain the movie "Samurai Cop"', () => {
        cy.contains('Samurai Cop').should('have.length', 1);
    })

    it('Should contain the movie "Roar"', () => {
        cy.contains('Roar').should('have.length', 1);
    })

    it('Should contain the movie "The Room"', () => {
        cy.contains('The Room').should('have.length', 1);
    })

    it('Should contain the movie "The Last Vampire On Earth"', () => {
        cy.contains('The Last Vampire On Earth').should('have.length', 1);
    })

    it('Should contain the movie "Spookies"', () => {
        cy.contains('Spookies').should('have.length', 1);
    })

    it('Should contain the movie "Ice Cream Man"', () => {
        cy.contains('Ice Cream Man').should('have.length', 1);
    })

    it('Should go to details page and check if the header is correct', () => {
        cy.server()
        cy.route('POST', 'http://localhost:3000/graphql', 'fixture:getMovieFixture.json');
        cy.get('#movie-title').click();
        cy.get('.MuiTypography-h3').contains("Miami Connection");
    })

    it('Should have correct quote', () => {
        cy.get('.MuiTypography-subtitle1').contains("My father");
    })

    it('Should have correct description', () => {
        cy.get('.MuiGrid-grid-sm-9 > .MuiGrid-root > :nth-child(4)').contains("ninjas");
    })

    it('Should have correct take', () => {
        cy.get('.MuiGrid-root > :nth-child(6)').contains("Taekwondo");
    })

    it('Should have correct runtime', () => {
        cy.get('.MuiGrid-grid-sm-3 > .MuiGrid-root > :nth-child(2)').contains("84");
    })

    it('Should have correct budget', () => {
        cy.get('.MuiGrid-grid-sm-3 > .MuiGrid-root > :nth-child(3)').contains("1");
    })

    it('Should have correct genre', () => {
        cy.get('.MuiGrid-grid-sm-3 > .MuiGrid-root > :nth-child(4)').contains("Arts");
    })

    it('Should have trailer widget visible', () => {
        cy.get('#widget2').should('be.visible');
    })

    it('Should be able to go back to main page', () => {
        cy.server()
        cy.route('POST', 'http://localhost:3000/graphql', 'fixture:allMoviesFixture.json')
        cy.get('.MuiButtonBase-root').click();
        cy.get('.main-title').contains('Ｔｒｅｎｄｉｎｇ');
    })




})
