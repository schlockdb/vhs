module.exports = {
    "env": {
        "browser": true,
        "es2020": true,
        "node": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:react/recommended"
    ],
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 11,
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    "rules": {
        "object-curly-spacing": 0,
        "space-before-function-paren": 0,
        "eol-last": 0,
        "no-trailing-spaces": 0
    },
    "parser": "babel-eslint",
    "settings": {
        "react": {
            "version": "detect"
        }
    }
};
