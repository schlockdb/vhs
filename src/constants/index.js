/**
 * Centralized place to save all the global variables
 *
 * @file   constants
 * @author Derek Diaz Correa
 * @since  8.15.2020
 */

export const GRAPHQL_BASE_URL_LOCAL = "http://localhost:3000/graphql";
export const GRAPHQL_BASE_URL_DEV = "https://server-dev.schlockdb.com/graphql";
export const GRAPHQL_BASE_URL_PROD = "https://server.schlockdb.com/graphql";


//Services
export const GOOGLE_PLAY = "Google Play";
export const YOUTUBE = "YouTube";
export const IMDB_TV = "IMDb TV";
export const ITUNES = "iTunes";
export const PRIME_VIDEO = "Prime Video";
export const TUBI = "tubi";
export const SHOUT_FACTORY_TV = "Shout Factory TV";
export const VUDU = "VUDU";

