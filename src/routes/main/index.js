/**
 * Routes - Index
 *
 * @file   index.js
 * @author Derek Diaz Correa
 * @since  8.15.2020
 */
import React, { useEffect } from 'react';
import Grid from "@material-ui/core/Grid/Grid";
import isNil from "../../utils/isNil";
import Lottie from "lottie-react";
import { useDispatch, useSelector } from "react-redux";
import loading from "../../assets/loading.json";

import { getSchlockData } from "../../redux/actions/schlockActions";

import "./main.css";
import { MovieTitle } from "../../components/movieTitle";
import { Intro } from "../../components/intro";


function generateList (data) {
    if (!isNil(data)) {
        return data.map((movie, index) => (
            <MovieTitle key={index} title={movie.title} cover={movie.cover} genre={movie.genre} id={movie.id} />
        ));
    }
}

function Index () {
    //Load data
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(getSchlockData());
    }, [dispatch]);

    const schlock = useSelector(
        state => state.schlockReducer
    );
    if (isNil(schlock) || schlock.loading) {
        return <div>
            <Lottie className="loading-anim" animationData={loading} />;
        </div>;
    } else {
        return (
            <div>
                <Intro/>
                <h1 className="main-title">Ｔｒｅｎｄｉｎｇ</h1>
                <Grid container spacing={4} className="movie-grid">
                    {generateList(schlock.data)}
                </Grid>
            </div>
        );
    }
}

export default Index;
