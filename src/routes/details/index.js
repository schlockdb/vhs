/**
 * Routes - Index
 *
 * @file   index.js
 * @author Derek Diaz Correa
 * @since  8.15.2020
 */
import React, { useEffect } from 'react';
import Grid from "@material-ui/core/Grid/Grid";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import Button from "@material-ui/core/Button";
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

import { DetailsSidebar } from "../../components/detailsSidebar";
import { DetailsPanel } from "../../components/detailsPanel";
import { getSingleSchlockData } from "../../redux/actions/schlockDetailsActions";

import "./details.css";
import isNil from "../../utils/isNil";

const Details = ({ match }) => {
    //Load data
    const dispatch = useDispatch();
    useEffect(() => {
        if (!isNil(match.params.id)) {
            dispatch(getSingleSchlockData(match.params.id));
        }
    }, [dispatch]);

    const schlock = useSelector(
        state => state.schlockDetailsReducer
    );

    if (isNil(schlock) || schlock.loading) {
        return <div/>;
    } else {
        let { title, cover, quote, description, runtime, budget, take, trailer, genre, stream } = schlock.data;
        return (
            <div>
                <Grid container spacing={2} className="details-container">
                    <Grid item xs={12}>
                        <Link to={`/`}><Button className="details-back"><ArrowBackIcon/> Back</Button></Link>
                    </Grid>
                    <Grid item xs={12}>
                        <div className="details-title">
                            ░▒▓ {title} ▓▒░
                        </div>
                        <div className="details-quote">
                            &quot;{quote}&quot;
                        </div>
                    </Grid>
                    <Grid item sm={3} xs={12}>
                        <DetailsSidebar title={title} cover={cover} runtime={runtime} budget={budget} genre={genre}/>
                    </Grid>
                    <Grid item sm={9} xs={12}>
                        <DetailsPanel description={description} take={take}
                            trailer={trailer} stream={stream}/>
                    </Grid>
                </Grid>
            </div>
        );
    }
};

export default Details;
