import { GRAPHQL_BASE_URL_LOCAL, GRAPHQL_BASE_URL_PROD } from "../constants";

export default function envGraphQL (env) {
    return (env === "development" ? GRAPHQL_BASE_URL_LOCAL : GRAPHQL_BASE_URL_PROD);
}
