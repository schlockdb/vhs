/**
 * Redux
 *
 * @file   index.js
 * @author Derek Diaz Correa
 * @since  8.15.2020
 */
import { combineReducers } from "redux";
import schlockReducer from "./reducers/schlockReducers";
import schlockDetailsReducer from "./reducers/schlockDetailsReducer";

const reducers = combineReducers({
    schlockReducer,
    schlockDetailsReducer
});

export default reducers;
