/**
 * Schlock Redux - Reducer
 *
 * @file   schlockReducer.js
 * @author Derek Diaz Correa
 * @since  8.15.2020
 */
import { actions } from '../actions/schlockDetailsActions';

const initialState = {
    loading: false,
    error: false,
    errorMessage: "",
    status: "",
    data: []
};

function schlockDetailsReducer (state = initialState, action) {
    switch (action.type) {
        case actions.SCHLOCK_DETAILS_REQUEST:
            return {
                ...state,
                loading: true,
                error: false,
                errorMessage: "",
                status: "",
                data: null
            };
        case actions.SCHLOCK_DETAILS_SUCCESS:
            return {
                ...state,
                loading: false,
                errorMessage: "",
                status: action.status,
                data: action.data
            };
        case actions.SCHLOCK_DETAILS_FAILED:
            return {
                ...state,
                loading: false,
                errorMessage: action.error,
                status: action.status,
                error: true
            };
        default:
            return state;
    }
}

export default schlockDetailsReducer;
