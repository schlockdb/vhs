/**
 * Schlock Redux - Reducer
 *
 * @file   schlockReducer.js
 * @author Derek Diaz Correa
 * @since  8.15.2020
 */
import { actions } from '../actions/schlockActions';

const initialState = {
    loading: false,
    error: false,
    errorMessage: "",
    status: "",
    data: []
};

function schlockReducer (state = initialState, action) {
    switch (action.type) {
        case actions.SCHLOCK_REQUEST:
            return {
                ...state,
                loading: true,
                error: false,
                errorMessage: "",
                status: "",
                data: null
            };
        case actions.SCHLOCK_SUCCESS:
            return {
                ...state,
                loading: false,
                errorMessage: "",
                status: action.status,
                data: action.data
            };
        case actions.SCHLOCK_FAILED:
            return {
                ...state,
                loading: false,
                errorMessage: action.error,
                status: action.status,
                error: true
            };
        default:
            return state;
    }
}

export default schlockReducer;
