/**
 * Schlock Redux - Actions
 *
 * @file   schlockActions.js
 * @author Derek Diaz Correa
 * @since  8.15.2020
 */
import { request, gql } from 'graphql-request';
import envGraphQL from "../../utils/enviroment";

export const actions = {
    SCHLOCK_REQUEST: 'SCHLOCK_REQUEST',
    SCHLOCK_SUCCESS: 'SCHLOCK_SUCCESS',
    SCHLOCK_FAILED: 'SCHLOCK_FAILED'
};


export function schlockRequest () {
    return { type: actions.SCHLOCK_REQUEST };
}

export function schlockSuccess (data) {
    return { type: actions.SCHLOCK_SUCCESS, data: data };
}

export function schlockFailed (error) {
    return { type: actions.SCHLOCK_FAILED, error };
}

export const getSchlockData = () => (dispatch) => {
    dispatch(schlockRequest());
    const query = gql`
        {
            allMovies {
                id
                title
                genre
                cover
            }
        }
    `;
    request(envGraphQL(process.env.NODE_ENV), query).then((data) => {
        console.log("Data:", data);
        dispatch(schlockSuccess(data.allMovies));
    }).catch(function (error) {
        console.log(error);
        dispatch(schlockFailed(error));
    });
};
