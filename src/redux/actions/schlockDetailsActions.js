/**
 * Schlock Redux - Actions
 *
 * @file   schlockActions.js
 * @author Derek Diaz Correa
 * @since  8.15.2020
 */
import {request, gql} from 'graphql-request';
import envGraphQL from "../../utils/enviroment";

export const actions = {
    SCHLOCK_DETAILS_REQUEST: 'SCHLOCK_DETAILS_REQUEST',
    SCHLOCK_DETAILS_SUCCESS: 'SCHLOCK_DETAILS_SUCCESS',
    SCHLOCK_DETAILS_FAILED: 'SCHLOCK_DETAILS_FAILED'
};


export function schlockDetailsRequest() {
    return {type: actions.SCHLOCK_DETAILS_REQUEST};
}

export function schlockDetailsSuccess(data) {
    return {type: actions.SCHLOCK_DETAILS_SUCCESS, data: data};
}

export function schlockDetailsFailed(error) {
    return {type: actions.SCHLOCK_DETAILS_FAILED, error};
}

export const getSingleSchlockData = (id) => (dispatch) => {
    dispatch(schlockDetailsRequest());

    const variables = {
        id: id.toString()
    };

    const query = gql`
        query getMovie($id: ID!) {
            getMovie(id: $id){
                title
                genre
                director
                runtime
                budget
                cover
                quote
                description
                take
                trailer
                stream{
                    service
                    url
                    paid
                    sub
                }
            }
        }
    `;
    request(envGraphQL(process.env.NODE_ENV), query, variables).then((data) => {
        console.log("Data:", data);
        dispatch(schlockDetailsSuccess(data.getMovie));
    }).catch(function (error) {
        console.log(error);
        dispatch(schlockDetailsFailed(error));
    });
};
