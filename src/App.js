/**
 * App
 *
 * @file   App.js
 * @author Derek Diaz Correa
 * @since  8.15.2020
 */

import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Main from "./routes/main";
import Details from "./routes/details";
import Header from "./components/header/";
import 'fontsource-roboto';

import "./App.css";
import { Footer } from "./components/footer";
import Metadata from "./components/metadata";

function App () {
    return (
        <Router>
            <div className="background">
                <Metadata/>
                <Header/>
                <div className="main-content">
                    <Route exact path="/" component={Main}/>
                    <Route path="/details/:id" component={Details}/>
                </div>
                <Footer/>
            </div>
        </Router>
    );
}

export default App;
