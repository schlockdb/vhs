/**
 * Generate a single movie title for the main screen
 *
 * @file   movieTitle.js
 * @author Derek Diaz Correa
 * @since  8.16.2020
 */
import React from 'react';
import Grid from "@material-ui/core/Grid/Grid";
import { Link } from "react-router-dom";
import { ParallaxHover } from "react-parallax-hover";

import './movieTitle.css';
import PropTypes from "prop-types";

export const MovieTitle = ({ id, cover, title, genre }) => {
    return (
        <Grid id={"movie-title"} item xs>
            <Link to={`/details/${id}`}>
                <div className="movie-wrapper">
                    <ParallaxHover width={200} height={360} shine={0} rotation={9} shadow={6}
                        borderRadius={10}>
                        <div className="movie-card">
                            <img
                                src={cover}
                                title={title}
                                alt={title}
                                className="movie-poster"
                            />
                            <div className="movie-title">{title}</div>
                            <div className="movie-genre">{genre}</div>
                        </div>
                    </ParallaxHover>
                </div>
            </Link>
        </Grid>
    );
};

MovieTitle.propTypes = {
    id: PropTypes.string,
    cover: PropTypes.string,
    title: PropTypes.string,
    genre: PropTypes.array
};
