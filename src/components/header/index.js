/**
 * Generate header
 *
 * @file   header.js
 * @author Derek Diaz Correa
 * @since  8.15.2020
 */
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import './header.css';


const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1
    },
    menuButton: {
        marginRight: theme.spacing(2)
    },
    title: {
        flexGrow: 1,
        textAlign: 'left'
    },
    button: {
        margin: theme.spacing(1)
    },
    appBar: {
        background: '#000'
    }
}));

export default function Header () {
    const classes = useStyles();

    return (
        <div className={classes.root} id={"SHeader"}>
            <AppBar position="static" className={"appBar"}>
                <Toolbar>
                    <Typography className={classes.title} id={"SHeader-Title"}>
                        ░▒▓ Ｓｃｈｌｏｃｋ ＤＢ ▓▒░
                    </Typography>
                </Toolbar>
            </AppBar>
        </div>
    );
}
