/**
 * Generate footer
 *
 * @file   footer.js
 * @author Derek Diaz Correa
 * @since  8.24.2020
 */
import React from 'react';


import './stickyFooter.css';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import Typography from "@material-ui/core/Typography";



export const Footer = () => {
    return (
        <footer>
            <BottomNavigation
                className="footer-main"
            >
                <Typography id={"Footer-Title"} className="footer-title">
                    Ｓｃｈｌｏｃｋ ＤＢ ▓ Ｃｏｐｙｒｉｇｈｔ░２０２０
                </Typography>
            </BottomNavigation>
        </footer>
    );
};

