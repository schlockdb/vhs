/**
 * Generate metadata
 *
 * @file   metadata.js
 * @author Derek Diaz Correa
 * @since  8.15.2020
 */
import React from 'react';
import {Helmet} from "react-helmet";
import favicon from '../../assets/favicon.ico';
import siteManifest from '../../assets/site.webmanifest';
import appleIcon from '../../assets/apple-touch-icon.png';
import faviconLg from '../../assets/favicon-32x32.png';
import faviconMd from '../../assets/favicon-16x16.png';
import safariIcon from '../../assets/safari-pinned-tab.svg';

export default function Metadata() {
    return (
        <Helmet>
            <meta charSet="utf-8"/>
            <link rel="icon" href={favicon}/>
            <meta name="viewport" content="width=device-width, initial-scale=1"/>
            <meta name="theme-color" content="#000000"/>
            <meta
                name="description"
                content="░▒▓ Ｓｃｈｌｏｃｋ ＤＢ ▓▒░"
            />
            <link rel="apple-touch-icon" sizes="180x180" href={appleIcon}/>
            <link rel="icon" type="image/png" sizes="32x32" href={faviconLg}/>
            <link rel="icon" type="image/png" sizes="16x16" href={faviconMd}/>
            <link rel="manifest" href={siteManifest}/>
            <link rel="mask-icon" href={safariIcon} color="#5bbad5"/>
            <meta name="msapplication-TileColor" content="#da532c"/>
            <meta name="theme-color" content="#ffffff"/>
            <title>░▒▓ Ｓｃｈｌｏｃｋ ＤＢ ▓▒░</title>
        </Helmet>
    );
}