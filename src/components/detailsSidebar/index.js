/**
 * Generate Sidebar for details page
 *
 * @file   detailsSidebar.js
 * @author Derek Diaz Correa
 * @since  8.16.2020
 */
import React from 'react';
import Grid from "@material-ui/core/Grid/Grid";

import './detailsSidebar.css';
import PropTypes from "prop-types";

export const DetailsSidebar = ({ cover, title, runtime, budget, genre }) => {

    const renderGenre = (genres) =>{
        if (!genre) return "";

        return genres.map((genre, index) => (
          <div className="details-genre" key={index}>{genre}</div>
        ));
    }

    return (
        <Grid
            container
            direction="column"
            justify="flex-start"
            alignItems="flex-start"
            className="details-side-box"
        >
            <div className="poster-wrapper">
                <img
                    src={cover}
                    title={title}
                    alt={title}
                    className="details-side-poster"
                />
            </div>
            <div className="details-side-text">
                <b>Runtime:</b> {runtime} Minutes
            </div>
            <div className="details-side-text">
                <b>Budget:</b> {budget}
            </div>
            <div className="details-side-text">
                <b>Genre:</b> {renderGenre(genre)}
            </div>
        </Grid>
    );
};

DetailsSidebar.propTypes = {
    cover: PropTypes.string,
    title: PropTypes.string,
    runtime: PropTypes.number,
    budget: PropTypes.string,
    genre: PropTypes.array
};
