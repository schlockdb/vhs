/**
 * Generate Sidebar for details page
 *
 * @file   streamPanel.js
 * @author Derek Diaz Correa
 * @since  9.21.2020
 */
import React from 'react';
import PropTypes from 'prop-types';

import './streamPanel.css';
import Typography from '@material-ui/core/Typography/Typography';
import isNil from '../../utils/isNil';
import { GOOGLE_PLAY, IMDB_TV, ITUNES, PRIME_VIDEO, SHOUT_FACTORY_TV, TUBI, VUDU, YOUTUBE } from '../../constants';

import GooglePlayImg from '../../assets/services/google-play.jpg';
import IMBbTVImg from '../../assets/services/imdb-tv.jpg';
import ItunesImg from '../../assets/services/itunes.jpg';
import PrimeVideoImg from '../../assets/services/prime-video.jpg';
import ShoutTvImg from '../../assets/services/shout-tv.jpg';
import TubiImg from '../../assets/services/tubi.jpg';
import VuduImg from '../../assets/services/vudu.jpg';
import YouTubeImg from '../../assets/services/youtube.jpg';
import Grid from '@material-ui/core/Grid';

export const StreamPanel = ({ stream }) => {
  console.log(stream);
  if (isNil(stream) || '') {
    return 'EMPTY!';
  } else {
    return (
      <div>
        <Typography variant="h5" className="stream-header">
          Where to watch:
        </Typography>
        <Grid container>
          {generateStreamServices(stream)}
        </Grid>
      </div>
    );
  }
};

const generateStreamServices = (stream) => {
  return stream.map((stream, index) => (

    <Grid item sm={12} key={index}>
      <div className="stream-box">
        <Grid container>
          <Grid item xs={12} sm={6}>
            <a href={stream.url}>
              <img
                src={generateServiceIcon(stream.service)}
                title={stream.service}
                alt={stream.service}
                className="stream-img"
              />
            </a>
          </Grid>
          <Grid item sm={6}>
            <div>{stream.service}</div>
            <div>Is it free?: {generateServiceBool(stream.paid)}</div>
            <div>Does it require a subscription?: {generateServiceBool(!stream.sub)}</div>
          </Grid>
        </Grid>
      </div>
    </Grid>
  ));
};


const generateServiceIcon = (serviceName) => {
  switch (serviceName) {
    case GOOGLE_PLAY:
      return GooglePlayImg;
    case IMDB_TV:
      return IMBbTVImg;
    case ITUNES:
      return ItunesImg;
    case PRIME_VIDEO:
      return PrimeVideoImg;
    case SHOUT_FACTORY_TV:
      return ShoutTvImg;
    case TUBI:
      return TubiImg;
    case VUDU:
      return VuduImg;
    case YOUTUBE:
      return YouTubeImg;
    default:
      return '';
  }
};

const generateServiceBool = (serviceBool) => {
  if (serviceBool) {
    return 'NOPE!';
  } else {
    return 'YUP!';
  }
};

StreamPanel.propTypes = {
  stream: PropTypes.array,
};
