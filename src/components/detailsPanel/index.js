/**
 * Generate Sidebar for details page
 *
 * @file   detailsPanel.js
 * @author Derek Diaz Correa
 * @since  8.16.2020
 */
import React from 'react';
import Grid from '@material-ui/core/Grid/Grid';
import PropTypes from 'prop-types';

import './detailsPanel.css';
import Typography from '@material-ui/core/Typography/Typography';
import YouTube from 'react-youtube';
import isNil from '../../utils/isNil';
import { StreamPanel } from '../streamPanel';

export const DetailsPanel = ({ description, take, trailer, stream }) => {
  return (
    <div>
      <Grid
        container
        direction="column"
        justify="flex-start"
        alignItems="flex-start"
      >

        <div className="details-box">
          <Typography variant="h5" className="details-header">
            Description:
          </Typography>
          <Typography variant="body1" gutterBottom>
            {description}
          </Typography>
          <Typography variant="h5" className="details-header">
            SchlockDB&apos;s Take:
          </Typography>
          <Typography variant="body1" gutterBottom>
            {take}
          </Typography>
          {renderYoutube(trailer)}
        </div>
        {stream && stream.length > 0 &&
          <StreamPanel stream={stream}/>
        }
      </Grid>
    </div>
  );
};

function renderYoutube(id) {
  if (isNil(id) || '') {
    return '';
  } else {
    return (
      <div>
        <Typography variant="h5" className="details-header">
          Trailer:
        </Typography>
        <YouTube
          videoId={id}
          className="details-trailer"
        />
      </div>);
  }
}


DetailsPanel.propTypes = {
  quote: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
  take: PropTypes.string,
  trailer: PropTypes.string,
  stream: PropTypes.array,
};
