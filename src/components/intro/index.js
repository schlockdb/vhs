/**
 * Generate Sidebar for details page
 *
 * @file   detailsPanel.js
 * @author Derek Diaz Correa
 * @since  8.16.2020
 */
import React from 'react';
import Typical from 'react-typical';

import './intro.css';


export const Intro = () => {
    return (
        <div className="intro">
            <h1 className="intro-title">Ｗｅｌｃｏｍｅ  ｔｏ  ＳｃｈｌｏｃｋＤＢ</h1>
            <p>A curated list of movies that will:
                <Typical
                    steps={['make you laugh and cry at the same time.', 2000,
                        'make you question reality.', 2000,
                        'make you scream at the screen.', 2000,
                        'make you appreciate movies on a whole new level!', 2000
                    ]} 
                    loop={1}
                /></p>
        </div>

    );
};
