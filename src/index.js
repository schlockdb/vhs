/**
 * Index
 *
 * @file   index.js
 * @author Derek Diaz Correa
 * @since  8.15.2020
 */

import React from 'react';
import ReactDOM from 'react-dom';
import reducers from "./redux";
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import App from './App';


const store = createStore(reducers, composeWithDevTools(applyMiddleware(thunk)));


ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('root')
);
