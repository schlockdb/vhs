import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import renderer from 'react-test-renderer';
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);


const store = mockStore({})

import Details from "../../routes/details";
import {Provider} from "react-redux";

describe('Details Route Test Suite', function() {

    it('renders correctly', () => {
        const tree = renderer
            .create(<Provider store={store}><Details/></Provider> )
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});
