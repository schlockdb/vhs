import React from 'react';
import '@testing-library/jest-dom/extend-expect';

import envGraphQL from "../../utils/enviroment";
import {GRAPHQL_BASE_URL_LOCAL, GRAPHQL_BASE_URL_PROD} from "../../constants";

describe('Environment Test Suite', function() {

    it('should get local url', () => {
        expect(envGraphQL("development")).toBe(GRAPHQL_BASE_URL_LOCAL);
    });

    it('should get prod url', () => {
        expect(envGraphQL("production")).toBe(GRAPHQL_BASE_URL_PROD);
    });
});
