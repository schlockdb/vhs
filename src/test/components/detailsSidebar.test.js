import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import renderer from 'react-test-renderer';

import {DetailsSidebar} from "../../components/detailsSidebar";

describe('DetailsSidebar Test Suite', function() {

    it('renders correctly', () => {
        const tree = renderer
            .create(<DetailsSidebar title={"A"} genre={"A"} cover={"A"} budget={"A"} runtime={"A"}/>)
            .toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('should render genre correctly', function() {
        expect(render(<DetailsSidebar genre={"A"}/>).getByText("A")).toBeInTheDocument();
    });

    it('should render budget correctly', function() {
        expect(render(<DetailsSidebar budget={"C"} />).getByText("C")).toBeInTheDocument();
    });

    it('should render runtime correctly', function() {
        expect(render(<DetailsSidebar runtime={"D"} />).getByText("D Minutes")).toBeInTheDocument();
    });
});
