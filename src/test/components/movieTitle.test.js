import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import renderer from 'react-test-renderer';

import {MovieTitle} from "../../components/movieTitle";
import {Router} from "@material-ui/icons";

describe('MovieTitle Test Suite', function() {

    it('renders correctly', () => {
        const tree = renderer
            .create(<Router><MovieTitle id={1} title={"A"} genre={"A"} cover={"A"} /></Router>)
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
});
