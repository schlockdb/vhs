import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import renderer from 'react-test-renderer';

import {DetailsPanel} from '../../components/detailsPanel';

describe('DetailsPanel Test Suite', function() {

    it('renders correctly', () => {
        const tree = renderer
            .create(<DetailsPanel title={"1"} trailer={null} take={"1"} quote={"1"} description={"1"}/>)
            .toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('should render title correctly', function() {
        expect(render(<DetailsPanel title={"A"}/>).getByText("A")).toBeInTheDocument();
    });

    it('should render description correctly', function() {
        expect(render(<DetailsPanel description={"B"} />).getByText("B")).toBeInTheDocument();
    });

    it('should render quote correctly', function() {
        expect(render(<DetailsPanel quote={"someQUOTE!"} />).getByText("\"someQUOTE!\"")).toBeInTheDocument();
    });

    it('should render quote correctly', function() {
        expect(render(<DetailsPanel take={"D"} />).getByText("D")).toBeInTheDocument();
    });
});
