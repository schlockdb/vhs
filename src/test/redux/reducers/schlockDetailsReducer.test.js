import {actions} from "../../../redux/actions/schlockDetailsActions";
import schlockDetailsReducer from "../../../redux/reducers/schlockDetailsReducer";

describe('schlockDetailsReducer', () => {
    let initialState = {
        data: [],
        error: false,
        errorMessage: "",
        loading: false,
        status: "",
    }
    it('should return the initial state', () => {
        expect(schlockDetailsReducer(undefined, {})).toEqual(initialState);
    });


    let requestState = {
        data: null,
        error: false,
        errorMessage: "",
        loading: true,
        status: "",
    }
    it('should return the SCHLOCK_DETAILS_REQUEST state', () => {
        const requestAction = {
            type: actions.SCHLOCK_DETAILS_REQUEST,
        };
        expect(schlockDetailsReducer({}, requestAction)).toEqual(requestState);
    });

    let errorState = {
        error: true,
        errorMessage: "BOOM",
        loading: false,
        status: 404,
    }
    it('should return the SCHLOCK_DETAILS_FAILED state', () => {
        const errorAction = {
            type: actions.SCHLOCK_DETAILS_FAILED,
            error: "BOOM",
            status: 404
        };
        expect(schlockDetailsReducer({}, errorAction)).toEqual(errorState);
    });

    let successState = {
        errorMessage: "",
        loading: false,
        status: 200,
        data: "Data",
    }
    it('should return the SCHLOCK_DETAILS_SUCCESS state', () => {
        const successAction = {
            type: actions.SCHLOCK_DETAILS_SUCCESS,
            data: "Data",
            status: 200
        };
        expect(schlockDetailsReducer({}, successAction)).toEqual(successState);
    });

});