import schlockReducer from "../../../redux/reducers/schlockReducers";
import {actions} from "../../../redux/actions/schlockActions";

describe('schlockReducer', () => {
    let initialState = {
        data: [],
        error: false,
        errorMessage: "",
        loading: false,
        status: "",
    }
    it('should return the initial state', () => {
        expect(schlockReducer(undefined, {})).toEqual(initialState);
    });


    let requestState = {
        data: null,
        error: false,
        errorMessage: "",
        loading: true,
        status: "",
    }
    it('should return the SCHLOCK_REQUEST state', () => {
        const requestAction = {
            type: actions.SCHLOCK_REQUEST,
        };
        expect(schlockReducer({}, requestAction)).toEqual(requestState);
    });

    let errorState = {
        error: true,
        errorMessage: "BOOM",
        loading: false,
        status: 404,
    }
    it('should return the SCHLOCK_FAILED state', () => {
        const errorAction = {
            type: actions.SCHLOCK_FAILED,
            error: "BOOM",
            status: 404
        };
        expect(schlockReducer({}, errorAction)).toEqual(errorState);
    });

    let successState = {
        errorMessage: "",
        loading: false,
        status: 200,
        data: "Data",
    }
    it('should return the SCHLOCK_SUCCESS state', () => {
        const successAction = {
            type: actions.SCHLOCK_SUCCESS,
            data: "Data",
            status: 200
        };
        expect(schlockReducer({}, successAction)).toEqual(successState);
    });

});