import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import fetchMock from 'fetch-mock'
import {
    getSchlockData,
    schlockRequest,
    schlockFailed,
    schlockSuccess,
    actions
} from "../../../redux/actions/schlockActions";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('schlockReducers actions', () => {
    afterEach(() => {
        fetchMock.restore()
    })

    it('Running getSchlockData action', () => {
        fetchMock.getOnce('/graphql', {
            body: {
                "data": {
                    "allMovies": [{
                        "id": "1",
                        "title": "Miami Connection",
                        "genre": "Martial Arts",
                        "cover": "url"
                    }]
                }
            },
            headers: {'content-type': 'application/json'}
        })

        const expectedActions = [
            {type: actions.SCHLOCK_REQUEST}
        ]
        const store = mockStore({
            loading: false,
            error: false,
            errorMessage: "",
            status: "",
            data: []
        })

        store.dispatch(getSchlockData())
        expect(store.getActions()).toEqual(expectedActions)
    });

    it('Running schlockRequest action', () => {

        const expectedActions = [
            {type: actions.SCHLOCK_REQUEST}
        ]
        const store = mockStore({
            loading: false,
            error: false,
            errorMessage: "",
            status: "",
            data: []
        })

        store.dispatch(schlockRequest())
        expect(store.getActions()).toEqual(expectedActions)
    })

    it('Running schlockFailed action', () => {

        const expectedActions = [
            {
                type: actions.SCHLOCK_FAILED,
                error: "BOOM"
            }
        ]
        const store = mockStore({
            loading: false,
            error: false,
            errorMessage: "",
            status: "",
            data: []
        })

        store.dispatch(schlockFailed("BOOM"))
        expect(store.getActions()).toEqual(expectedActions)
    })

    it('Running schlockSuccess action', () => {

        const expectedActions = [
            {
                type: actions.SCHLOCK_SUCCESS,
                data: "data"
            }
        ]
        const store = mockStore({
            loading: false,
            error: false,
            errorMessage: "",
            status: "",
            data: []
        })

        store.dispatch(schlockSuccess("data"))
        expect(store.getActions()).toEqual(expectedActions)
    })
});