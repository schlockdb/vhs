import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import fetchMock from 'fetch-mock'
import {
    getSingleSchlockData,
    schlockDetailsFailed,
    schlockDetailsRequest,
    schlockDetailsSuccess,
    actions
} from "../../../redux/actions/schlockDetailsActions";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('schlockDetailsReducers actions', () => {
    afterEach(() => {
        fetchMock.restore()
    })

    it('Running getSingleSchlockData action', () => {
        fetchMock.getOnce('/graphql', {
            body: {
                "data": {
                    "getMovie": {
                        "id": "1",
                        "title": "Miami Connection",
                        "genre": "Martial Arts",
                        "cover": "url"
                    }
                }
            },
            headers: {'content-type': 'application/json'}
        })

        const expectedActions = [
            {type: actions.SCHLOCK_DETAILS_REQUEST}
        ]
        const store = mockStore({
            loading: false,
            error: false,
            errorMessage: "",
            status: "",
            data: []
        })

        store.dispatch(getSingleSchlockData(1))
        expect(store.getActions()).toEqual(expectedActions)
    });

    it('Running schlockDetailsRequest action', () => {

        const expectedActions = [
            {type: actions.SCHLOCK_DETAILS_REQUEST}
        ]
        const store = mockStore({
            loading: false,
            error: false,
            errorMessage: "",
            status: "",
            data: []
        })

        store.dispatch(schlockDetailsRequest())
        expect(store.getActions()).toEqual(expectedActions)
    })

    it('Running schlockDetailsFailed action', () => {

        const expectedActions = [
            {
                type: actions.SCHLOCK_DETAILS_FAILED,
                error: "BOOM"
            }
        ]
        const store = mockStore({
            loading: false,
            error: false,
            errorMessage: "",
            status: "",
            data: []
        })

        store.dispatch(schlockDetailsFailed("BOOM"))
        expect(store.getActions()).toEqual(expectedActions)
    })

    it('Running schlockDetailsSuccess action', () => {

        const expectedActions = [
            {
                type: actions.SCHLOCK_DETAILS_SUCCESS,
                data: "data"
            }
        ]
        const store = mockStore({
            loading: false,
            error: false,
            errorMessage: "",
            status: "",
            data: []
        })

        store.dispatch(schlockDetailsSuccess("data"))
        expect(store.getActions()).toEqual(expectedActions)
    })
});