```
 _    __    __  __   _____
| |  / /   / / / /  / ___/
| | / /   / /_/ /   \__ \ 
| |/ /   / __  /   ___/ / 
|___/   /_/ /_/   /____/  
                          
```
# SchlockDB Frontend

## Project Description

This is an experiment project that I made just to play with the MERN (GraphQL, Express, React, Node) Stack. 

In this project you can interact with a collection of Schlock Movies.

VHS is the React frontend for the backend which is [Betamax](https://gitlab.com/schlockdb/betamax).
## How to run

In the project directory, you can run:

```
npm install
npm run start
```
### 

Runs the app in the development mode.<br>
Open [http://localhost:3006](http://localhost:3006) to view it in the browser.

Do have in mind that this application needs to have its backend (Betamax) running. 

## Dependencies
This application won't work properly without its GraphQL backend server.

## Code Coverage Report
[https://schlockdb.gitlab.io/vhs/coverage/](https://schlockdb.gitlab.io/vhs/coverage/)

## Application Overview
The front end currently has 2 main GraphQL calls.
- allMovies - The Index page makes this call everytime it is rendered. We use this to simply show the list of movies.
- getMovie(id : ID!) - When we click on a single movie, we make this call to get more details about the single item. 

![Overview](./docs/SchlockDB-FrontEnd.png)

## TODO List
This is check list of items Derek needs to work on
- Add local mock server to allow app to run without a backend running locally
- Add user login support
- Filtering and search
- Animation to make transitions more seamless
- User profiles
- Like/Dislike 

### [SCHLOCKDB.COM](http://www.schlockdb.com)

