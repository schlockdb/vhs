FROM node:12

WORKDIR /app
COPY . .
RUN npm install react-scripts -g --silent
RUN yarn install
RUN yarn run build

RUN yarn global add serve

CMD ["serve", "-p", "80", "-s", "."]